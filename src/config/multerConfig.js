import multer from 'multer'
import storage from './Uploader'

export default multer({
  storage,
  onError: function (err, next) {
    console.log('error', err);
    next(err);}

  })

