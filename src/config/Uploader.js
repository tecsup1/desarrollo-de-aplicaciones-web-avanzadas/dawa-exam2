import cloudinary from 'cloudinary'
import cloudinaryStorage from 'multer-storage-cloudinary'

cloudinary.config({
  cloud_name: process.env.CLOUD_NAME,
  api_key: process.env.API_KEY,
  api_secret: process.env.API_SECRET
})

const storage = cloudinaryStorage({
  cloudinary,
  folder: "examen",
  allowedFormats: ["jpg", "png"],
  // transformation: [{ width: 500, height: 500, crop: "limit" }],
});

export default storage