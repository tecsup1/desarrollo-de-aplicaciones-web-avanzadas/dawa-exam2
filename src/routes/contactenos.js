import express from "express";
import sendSession from "#root/middleware/sendSession";
const route = express.Router();

route.get("/contactenos",(req, res, next) => {
    res.render("components/contactenos", {
        session:req.session.user
    })
  }
);

export default route;
