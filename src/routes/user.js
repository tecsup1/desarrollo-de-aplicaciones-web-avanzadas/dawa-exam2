import express from "express";

import { register, login, signOut } from "#root/controllers/userController";
import session from "#root/middleware/sessionInit";
import { redirectNews } from "#root/middleware/redirectNews";

const route = express.Router();

route.post("/register", register);

route.post("/login", login, session, redirectNews);

route.get("/login", (req, res) => {
  res.render("components/login", {session:req.session.user});
});

route.get("/register", (req, res) => {
  res.render("components/registro",{session: req.session.user});
});

route.get("/signout", signOut);

export default route;
