import express from "express";

import moment from 'moment'
moment.locale("es")
// Models import:
import New from "#root/models/new";
import Comment from "#root/models/commentModel";
import Commun from "../models/comunComment";
import multer from '../config/multerConfig'
const route = express.Router();



route.get("/news_listar", async (req, res, next) => {
    try {
        const commun = await Commun.find({}).sort({ createdAt: -1 }).lean()
        const sani = commun.map(i => ({ ...i, createdAt: moment(i.createdAt).fromNow() }))

        
        // moment().format('ll');   
        const my_news = await New.find({}).sort({ createdAt: -1 }).lean()
        const sani2 = my_news.map(i => ({...i, createdAt: moment(i.createdAt).format('ll')}))
        res.render("components/home", {
            my_news: sani2,
            session: req.session.user ? req.session.user : "",
            comments: sani
        });
    } catch (error) {
        next(error)
    }



});

// RUTA PARA IR A LA VISTA DE DETALLES DE CADA NOTICIA
route.get("/new_detail/:news_id", (req, res) => {
    // console.group(datos.news[0]);
    const new_id = req.params.news_id;

    // Encuentra la noticia usando su id:
    New.findById(new_id).exec((err, my_new) => {
        // console.log(my_new);
        if (err) {
            return res.status(400).json({
                ok: false,
                err,
            });
        }
        // Encuentra los comentarios de la noticia actual
        Comment.find({ _new: new_id }).exec((err, comments_array) => {
            if (err) {
                console.log("mi error papu: ", err);
            }
            res.render("components/new_detail", {
                session: req.session.user,
                my_new: my_new,
                comments_array: comments_array,
                news_id: new_id,
                user_id: req.session.user._id
            });
        });
    });
});

// RUTA DE LA PÁGINA PARA LA CREACIÓN DE NOTICIAS
route.get("/news_creation_page", (req, res) => {

    res.render("components/news_creation_page", {
        session: req.session.user,
        // my_news: my_news,
    });
});

// RUTA PARA CREAR UNA NUEVA NOTICIA
route.post("/news_crear", multer.single('imagen'), (req, res) => {
    const body = req.body;
    const noticia_string = req.body.body_editor;// captura información del wysiwyg

    const today = moment(Date.now()).format("DD/MM/YYYY");

    let noticia = new New({
        title: body.title,
        body: [req.file.secure_url, body.body2],
        category: body.category,
        creaction_date: today,
        body_editor: noticia_string,
    });
    noticia.save((err, noticiaDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err,
            });
        }
        res.redirect("/news_listar");
    });
});

// RUTA PARA BORRAR LA NOTICIA SELECCIONADA
route.post("/news_remove/:news_id", (req, res) => {
    const news_id = req.params.news_id;
    New.findByIdAndDelete(news_id, (err, usuarioBorrado) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err,
            });
        }
        res.redirect("/news_listar");
    });
});

// RUTA PARA IR A LA PÁGINA DE ACTUALIZACIÓN DE NOTICIA
route.get("/news_update_page/:news_id", (req, res) => {
    const news_id = req.params.news_id;

    New.findById(news_id).exec((err, my_new) => {
        console.log(my_new);
        if (err) {
            return res.status(400).json({
                ok: false,
                err,
            });
        }

        res.render("components/news_creation_page", {
            session: req.session.user,
            my_new: my_new,
        });
    });
});

// RUTA PARA ACTUALIZAR NOTICIA
route.post("/news_update/:news_id", (req, res) => {
    const news_id = req.params.news_id;
    const body = req.body;
    const arrayBody = [req.body.body1, req.body.body2];
    body.body = arrayBody;
    console.log("tu body papu: ", body)
    //   const today = moment(Date.now()).format("DD/MM/YYYY");
    delete body.body1;
    delete body.body2;


    // console.log("body : ", body);
    New.findByIdAndUpdate(news_id, body, { new: true }, (err, usuarioDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err,
            });
        }
        res.redirect("/news_listar");
    });
});


route.post("/commun", async (req, res, next) => {
    try {
        const comment = new Commun(req.body)
        const save = await comment.save()
        res.json(save)
    } catch (err) {
        res.json({
            err: err.message
        })
    }
})


// RUTA PARA PROBAR LO RELACIONADO CON EL WYSIWYG
route.post('/getRichText', async (req, res) => {
    // const fillComment = {"ops":[{"insert":"Hello World!\nSome initial "},{"attributes":{"bold":true},"insert":"bold"},{"insert":" text\n\n"}]}
    const noticia_object = JSON.parse(req.body.body_editor);
    const noticia_string = req.body.body_editor;
    console.log('type of: ', typeof (noticia_object))
    console.log('accediendo a sin modificar:', noticia_object.ops);

    // var myString = JSON.stringify(new_comment);
    // var myObject = JSON.parse(myString);

    // console.log('string: ',myString);
    // console.log('object: ', myObject);
});

export default route;
