import express from "express";
var moment = require("moment");

const route = express.Router();

// Models import:
import New from "#root/models/new";
import Comment from "#root/models/commentModel";
import User from "#root/models/user";

// RUTA PARA CREAR NUEVO COMENTARIO
route.post("/comment_create/:news_id/:user_id", async (req, res) => {
    const news_id = req.params.news_id;
    const user_id = req.params.user_id;
    const new_comment = req.body.new_comment;

    try {

        // Encuentra el usuario para extraer su nombre
        const my_user = await User.findById(user_id).exec();

        const new_comment_object = new Comment();
        new_comment_object._new = news_id;
        new_comment_object._user = user_id;
        new_comment_object.body = new_comment;
        new_comment_object.nombre_user = my_user.nombre;
        new_comment_object.date = moment(Date.now()).format("DD/MM/YYYY");
        // Graba el nuevo comentario:
        await new_comment_object.save();

        // Encuentra los comentarios de la noticia actual:
        const comments = await Comment.find({ _new: news_id }).exec();
        res.redirect("/new_detail/"+news_id);

    } catch (error) {
        console.log("Hubo el siguiente error: ", error);
    }

    // });
});


export default route;
