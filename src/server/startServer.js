// DEPENDENCIAS
import express from "express";
import cors from "cors";
import session from '#root/middleware/sessionInit.js'
import path from "path";

// data de json:


// import ROUTES
import User from '#root/routes/user'
import News from '#root/routes/news'
import Contactenos from '#root/routes/contactenos'
import CommentsRoute from '#root/routes/commentsRoute'

// MIDDLEWARES
const app = express();
const PORT = process.env.PORT || 4000;

app.use(session)
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());

app.use("/public", express.static(path.join(path.resolve(), "/node_modules/bootswatch/dist/lux")));
app.use("/public", express.static(path.join(path.resolve(), "/public")));
app.set("view engine", "pug");

app.set('views', './src/views'); // indicando donde está la carpeta view



// ROUTES
app.use(User)
app.use(News)
app.use(Contactenos)
app.use(CommentsRoute)



app.get("/aea", (req, res) => {
  req.session = req.session ? req.session+1 : 1
  res.send(`acasc${req.session}`)
});

// HANDLE ERRORS
app.use((err, req, res, next) => {
  res.render("components/error", {err: err.message})
});

// UP SERVER
app.listen(PORT, () => {
  console.log(`LISTENING ON PORT: ${PORT}`);
});
