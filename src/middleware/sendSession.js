//red -- redirect
// ren --> render

export default (r, url, payload = {}) => {
  return function (req, res, next) {
    if (r == "red") {
      res.redirect(url);
    } else {
      res.render(url, { ...payload, session: req.session });
    }
  };
};
