

export function redirectNews(req, res, next) {
  req.session.user = req.user
  res.redirect("/news_listar")  
  // ACA PODEMOS GUARDAR LOS DATOS EN SESSION Y VALIDAR DESDE LAS VISTAS SI ESTA LOGEADO O NO
}