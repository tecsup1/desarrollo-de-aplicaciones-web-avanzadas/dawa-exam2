import session from 'express-session'
import MongoDBStore from 'connect-mongodb-session'

const storeSession = MongoDBStore(session)
const store = new storeSession({
  uri: process.env.DATA_BASE,
})

const sessionInit = session({
  secret: process.env.SESSION_SECRET,
  store,
  resave: true,
  saveUninitialized: false
})

export default sessionInit