import User from "#root/models/user"


export async function register(req, res, next) {
  try {
    const user = await User.create(req.body)
    res.redirect("/login")
  } catch (error) {
    next(error)
  }
}

export async function login(req, res, next) {
  try {
    const user = await User.findOne({
      email: req.body.email
    })

    if(!user) return next(new Error("No existe usuario"))

    const isValid = await user.verifyPassword(req.body.password)

    if (!isValid) return next(new Error("Contraseña incorrecta"))
    else{
      const { _id, email, nombre, isAdmin} = user
      req.user = {_id, email, nombre, isAdmin}
      return next()
    } 
    
  } catch (error) {
    next(error)
  }
}


export function signOut(req, res) {
  req.session.destroy()
  res.redirect("/news_listar")
}