import mongoose from 'mongoose'
import mongooseBrypt from 'mongoose-bcrypt'

const userSchema = new mongoose.Schema({
  nombre: String,
  email: {
    type: String,
    required: true,
    unique: true
  },
  isAdmin: {
    type: Boolean,
    default: false
  }
}, { timestamps: true })


userSchema.plugin(mongooseBrypt)
const User = mongoose.model("User", userSchema)

export default User