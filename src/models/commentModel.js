import mongoose from 'mongoose'


const commentSchema = new mongoose.Schema({
  _user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true
  },
  _new: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "New",
    required: true
  },
  body: {
    type: String,
    required: true
  },
  nombre_user: {
    type: String,
    ref: "User",
    required: true
  },
  date:{
    type: String,
    required: true
  }
})


const Comment = mongoose.model("Comment", commentSchema)

export default Comment