import mongoose from 'mongoose'

const newSchema = new mongoose.Schema({
  title: String,
  subtitle: String,
  body: [String],
  body_editor: String,
  category: String,
  creation_date: String,
}, { timestamps: true })


const New = mongoose.model('New', newSchema)

export default New