import mongoose from 'mongoose'


const commentSchema = new mongoose.Schema({
  _user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true
  },
  body: {
    type: String,
    required: true
  },
  nombre_user: {
    type: String,
    ref: "User",
    required: true
  }
}, { timestamps: true })


const Commun = mongoose.model("Commun", commentSchema)

export default Commun