import mongoose from 'mongoose';

mongoose.connect(process.env.DATA_BASE, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true })

mongoose.connection.on('connected', function () {
  console.log("---------> DB CONNECTED");
});