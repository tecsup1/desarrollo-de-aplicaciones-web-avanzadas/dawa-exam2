const path = require("path")
const nodeExternals = require("webpack-node-externals")

module.exports = {
  entry: "./src/index.js",
  externals: [nodeExternals()],
  mode: "production",
  target: "node",

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "babel-loader",
      }
    ]
  },

  output: {
    filename: "bundle.js",
    path: path.resolve(__dirname, "dist")
  }
}