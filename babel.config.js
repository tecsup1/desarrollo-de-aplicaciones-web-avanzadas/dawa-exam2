module.exports = {
  presets: [
    [
      "@babel/preset-env",
      {
        targets: {
          node: "current",
          esmodules: false
        }
      }
    ]
  ],
  plugins: [
    [
      "module-resolver",
      {
        alias: {
          "#root": "./src"
        }
      }
    ]
  ]
}